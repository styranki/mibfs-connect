use btleplug::api::{bleuuid::BleUuid, Central, Manager as _, Peripheral as _, ScanFilter};
use btleplug::platform::{Adapter, Manager, Peripheral};

use std::io::Write;
use byteorder::{LittleEndian, BigEndian, WriteBytesExt, ReadBytesExt};

use std::error::Error;
use std::str::FromStr;
use futures::stream::StreamExt;

use std::time::Duration;
use tokio::time;


const SCALE_BT_ADDRESS: &str =  "5ccad3c28807";

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let manager = Manager::new().await.unwrap();

    let adapters = manager.adapters().await?;
    let central = adapters.into_iter().nth(0).unwrap();

    let mut events = central.events().await?;
    // start scanning for devices
    central.start_scan(ScanFilter::default()).await?;

    use btleplug::api::CentralEvent;


    while let Some(event) = events.next().await {
        match event {
            CentralEvent::ManufacturerDataAdvertisement {
                id,
                manufacturer_data,
            } => {
                println!(
                    "ManufacturerDataAdvertisement: {:?}, {:?}",
                    id, manufacturer_data
                );
            }
            CentralEvent::ServiceDataAdvertisement { id, service_data } => {
                println!("ServiceDataAdvertisement: {:?}, {:?}", &id, &service_data);
                println!("{:?}", id);
                // Only react to data with bodycomp UUID
                let uuid = uuid::Uuid::from_str("0000181b-0000-1000-8000-00805f9b34fb").unwrap();
                match service_data.get(&uuid) {
                    Some(data) => {
                        dbg!(data);
                        let mut year = Vec::new();
                        year.write_u8(data[2]);
                        year.write_u8(data[3]);
                        let month = data[4];
                        let day = data[5];
                        println!("Date: {}.{}.{}", day, month, std::io::Cursor::new(year).read_u16::<LittleEndian>().unwrap());

                        let hours = data[6];
                        let minutes = data[7];
                        let seconds = data[8];
                        println!("Time: {}:{}:{}", hours, minutes, seconds);

                        let mut impedance = Vec::new();
                        impedance.write_u8(data[9]);
                        impedance.write_u8(data[10]);
                        println!("Impedance: {}", std::io::Cursor::new(impedance).read_u16::<LittleEndian>().unwrap());

                        let mut weight = Vec::new();
                        weight.write_u8(data[11]);
                        weight.write_u8(data[12]);
                        println!("Weight: {}", std::io::Cursor::new(weight).read_u16::<LittleEndian>().unwrap() as f32 / 200.0);
                    },
                    _ => {},
                }
            }
            CentralEvent::ServicesAdvertisement { id, services } => {
                let services: Vec<String> =
                    services.into_iter().map(|s| s.to_short_string()).collect();
                println!("ServicesAdvertisement: {:?}, {:?}", id, services);
            }
            _ => {}
        }
    }


    // instead of waiting, you can use central.event_receiver() to fetch a channel and
    // be notified of new devices
    time::sleep(Duration::from_secs(2)).await;

    let scale = find_mibfs(&central).await.unwrap();

    scale.connect().await?;
    dbg!(&scale);
    scale.discover_services().await?;
    dbg!(scale.characteristics());

    Ok(())

}

async fn find_mibfs(central: &Adapter) -> Option<Peripheral> {
    for p in central.peripherals().await.unwrap() {
        let addr = p.properties()
            .await
            .unwrap()
            .unwrap()
            .address
            .to_string_no_delim();
        //if addr == "42ff7513ab6e"
        if &addr == SCALE_BT_ADDRESS
        {
            dbg!("Scale found");
            dbg!(p.properties().await.unwrap().unwrap().local_name);
            dbg!(SCALE_BT_ADDRESS);
            return Some(p);
        }
    }
    None
}
